# Input Tags

##
## Functionality requirements:

- Autocomplete functionality. Get the suggested items from a mocked endpoint.
- Allow multiple selection. 
- Use Jest for unit test the component.

```
The input text allows searching for countries.
If I start typing “Au” it should suggest “Austria, Australia” and allow me to pick one. 
Then I should be able to type again and add another option, keeping my first selection. 
I should be able to DELETE any previous selected items from the input field.
```

## Environment requirements:

- [Node.js](https://nodejs.org/en/)
- [npm](https://www.npmjs.com)
- [Webpack](https://webpack.github.io/)

## Installation:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install node openssl
npm install -g webpack
git clone https://mengyaw1@bitbucket.org/mengyaw1/input-tags.git ~/folder/
cd ~/folder/input-tags
npm install
npm start
```

## Run locally as development, with HMR enabled:
`npm start`

## Run as production:
`npm run start:prod`

Note that this includes building, which takes a while

## Build for production:
`npm run build`

## Responsive breakpoints:
```
[
  {
    name: 'xxs',
    px: 359
  },
  {
    name: 'xs',
    px: 480
  },
  {
    name: 's',
    px: 640
  },
  {
    name: 'm',
    px: 768
  },
  {
    name: 'l',
    px: 1024
  },
  {
    name: 'xl',
    px: 1244
  },
  {
    name: 'xxl',
    px: 1410
  }
]
```

### Examples
```scss
@include bp(s)      { } // Small and up
@include bp(s, m)   { } // Between small and medium
@include bp(1024)   { } // 1024px and up
@include bp(0, xxs) { } // Between 0 and xxs
```